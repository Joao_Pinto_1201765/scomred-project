function show(xpto) {
    var x = document.getElementById(xpto);
    x.style.display = "block";
    x.style.zIndex = 1;

    document.getElementById("result-createAcc").classList.remove('alert');
    document.getElementById("result-createAcc").classList.remove('alert-success');
    document.getElementById("result-createAcc").classList.remove('alert-danger');
    document.getElementById("result-invoice").classList.remove('alert');
    document.getElementById("result-invoice").classList.remove('alert-success');
    document.getElementById("result-invoice").classList.remove('alert-danger');
}

function showG(xpto) {
    var x = document.getElementById(xpto);
    x.style.display = "grid";
    x.style.gridTemplateRows = "1fr 0.5fr 0.5fr";
    x.style.gridTemplateColumns = "1fr 1fr";
}

function showIntro(xpto) {
    var x = document.getElementById(xpto);
    x.style.display = "grid";
    x.style.gridTemplateRows = "0.05fr 1fr 1fr";
    x.style.zIndex = 1;
}

function hide(xpto) {
    var x = document.getElementById(xpto);
    var resultAccount = document.getElementById('result-createAcc');
    var resultInvoice = document.getElementById('result-invoice');
    x.style.display = "none"
    x.style.zIndex = -1;
    clear();
    resultAccount.innerHTML = '';
    resultInvoice.innerHTML = '';
}

function clear(){
    document.getElementById('vat').value = '';
    document.getElementById('clientVat').value = '';
    document.getElementById('firstName').value = '';
    document.getElementById('lastName').value = '';
    document.getElementById('address').value = '';
    document.getElementById('invDate').value = '';
    document.getElementById('monthSupply').value = '';
    document.getElementById('amount').value = '';
}

function errorClass() {
    document.getElementById("result-createAcc").classList.add('alert');
    document.getElementById("result-createAcc").classList.remove('alert-success');
    document.getElementById("result-createAcc").classList.add('alert-danger');
}

function addVat() {
    var request = new XMLHttpRequest();
    var vat = document.getElementById("vat")
    var data = "Vat=" + vat.value;

    if ( !(vat.value.toString().length == 9) ){
        errorClass();
        document.getElementById("result-createAcc").innerHTML = "Vat Number is not correct";
        return;
    }

    request.onload = function okCase() {
        document.getElementById("result-createAcc").classList.add('alert');
        document.getElementById("result-createAcc").classList.remove('alert-danger');
        document.getElementById("result-createAcc").classList.add('alert-success');
        if ( !(this.status == 200) ){
            errorClass();
        }
        document.getElementById("result-createAcc").innerHTML = this.responseText;
    };

    request.ontimeout = function timeoutCase() {
        errorClass();
        document.getElementById("result-createAcc").innerHTML = "Account was not created. Timeout.";
    };

    request.onerror = function errorCase() {
        errorClass();
        document.getElementById("result-createAcc").innerHTML = "Account was not created. An error has occurred! " + this.responseText;
    };

    request.open("POST", "/cgi-bin/pumCreateAccount.bash", true);
    request.timeout = 5000;
    request.send(data);
}

function errorClassInvoice() {
    document.getElementById("result-invoice").classList.add('alert');
    document.getElementById("result-invoice").classList.remove('alert-success');
    document.getElementById("result-invoice").classList.add('alert-danger');
}

function addInvoice() {
    var request = new XMLHttpRequest();
    var vat = document.getElementById("clientVat").value;
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var address = document.getElementById("address").value;
    var invDate = document.getElementById("invDate").value;
    var monthSupply = document.getElementById("monthSupply").value;
    var amount = document.getElementById("amount").value;

    if ( !(vat.toString().length == 9) ){
        document.getElementById("result-invoice").classList.add('alert');
        document.getElementById("result-invoice").classList.remove('alert-success');
        document.getElementById("result-invoice").classList.add('alert-danger');
        document.getElementById("result-invoice").innerHTML = "Vat Number is not correct";
        return;
    }

    if ( (vat == "") ) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "Missing Vat number";
        return;
    }

    if ( (firstName == "") ) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "Missing first name";
        return;
    }

    if ( (lastName == "") ) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "Missing last name";
        return;
    }

    if ( (address == "") ) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "Missing address";
        return;
    }

    if ( (invDate == "") ) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "Missing invoice date";
        return;
    }

    if ( (monthSupply == "") ) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "Missing month of supply";
        return;
    }

    if ( (amount == "") ) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "Missing amount";
        return;
    }

    if ( (amount < 0)) {
        errorClassInvoice();
        document.getElementById("result-invoice").innerHTML = "The amount can not be negative";
        return;
    }

    //var data = "Vat=" + vat + "&" + firstName + "&" + lastName + "&" + address + "&" + invDate + "&" + monthSupply + "&" + amount;
    var data = "vat="+ vat + "&firstName=" + firstName + "&lastname=" + lastName + "&address=" + address + "&invDate=" + invDate + "&monthSupply=" + monthSupply + "&amount=" + amount;

    request.onload = function okCase() {
        document.getElementById("result-invoice").classList.add('alert');
        document.getElementById("result-invoice").classList.remove('alert-danger');
        document.getElementById("result-invoice").classList.add('alert-success');
        if ( !(this.status == 200) ){
            errorClassInvoice();
        }
        document.getElementById("result-invoice").innerHTML = this.responseText;
    };

    request.ontimeout = function timeoutCase() {
        document.getElementById("result-invoice").classList.add('alert');
        document.getElementById("result-invoice").classList.remove('alert-success');
        document.getElementById("result-invoice").classList.add('alert-danger');
        document.getElementById("result-invoice").innerHTML = "Account was not created. Timeout.";
    };

    request.onerror = function errorCase() {
        document.getElementById("result-invoice").classList.add('alert');
        document.getElementById("result-invoice").classList.remove('alert-success');
        document.getElementById("result-invoice").classList.add('alert-danger');
        document.getElementById("result-invoice").innerHTML = "Account was not created. An error has occurred! " + this.responseText;
    };

    request.open("POST", "/cgi-bin/pumAddInvoice.bash", true);
    request.timeout = 5000;
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(data);
}
