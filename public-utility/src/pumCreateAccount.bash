#!/bin/bash
POST_CONTENT=/tmp/createAccount$$.tmp
cat > txt.txt

function error_response() {
  echo "Status: 400 Bad Request"
  echo "Content-type: text/plain"
  echo ""
  echo "ERROR: ${1}"
  rm -f $POST_CONTENT
  exit
}
###

if [ -n "$CONTENT_LENGTH" ]; then
  cat txt.txt > $POST_CONTENT;
else
  error_response "No content found on the request";
fi
###

if [ "$REQUEST_METHOD" != "POST" ]; then error_response "Invalid method. The only supported method is POST."; fi
###

NOME=$(cut -d "=" -f 2 $POST_CONTENT);
if [ -z "$NOME" ]; then error_response "Vat is empty"; fi

if [ ${#NOME} != 9 ]; then error_response "Vat is not correct"; fi

if [ -d /home/public-utility/database/"$NOME" ]; then error_response "VAT already registered"; fi
mkdir /home/public-utility/database/"$NOME"
###

rm -f $POST_CONTENT
echo "Content-type: text/plain"
echo ""
echo "Vat successfully registered"
###
