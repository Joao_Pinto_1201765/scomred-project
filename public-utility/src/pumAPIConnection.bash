#!/bin/bash
POST_CONTENT=/tmp/APIrequest$$.tmp

function error_response() {
  echo "Status: 400 Bad Request"
  echo "Content-type: text/plain"
  echo ""
  echo "ERROR: ${1}"
  rm -f $POST_CONTENT
  exit
}

####
if [ -n "$CONTENT_LENGTH" ]; then
  cat > $POST_CONTENT;
else
  error_response "No content found on the request";
fi
###

if [ "$REQUEST_METHOD" != "POST" ]; then
 error_response "Invalid method. The only supported method is POST.";
fi
###

VAT=$(cut -d "=" -f 2 $POST_CONTENT)

for i in /home/public-utility/database/"$VAT"/*; do
 INVOICE=$(echo "$i" | cut -d "/" -f 10)
 #INFO=$(cat $i | cut -d "=" -f 2)
 DATE=$(grep "InvDate=" $i)
 AMOUNT=$(grep "Amount=" $i)
 MONTH=$(grep "MonthSupply=" $i)
 ADDRESS=$(grep "Address=" $i | tr [:space:] _)
 echo $DATE"&"$AMOUNT"&"$MONTH"&"$ADDRESS >> temp$$.txt
 #echo "$INVOICE" >> temp$$.txt
 #cat $i >> temp$$.txt
 #cat $INFO >> temp$$.txt
done

REQUESTED_INFO=$(cat temp$$.txt)
echo "Content-type: text/plain"
echo ""
echo $REQUESTED_INFO
#cat temp$$.txt

rm -f $POST_CONTENT
rm -f temp$$.txt