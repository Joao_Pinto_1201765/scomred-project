#!/bin/bash
POST_CONTENT=/tmp/createAccount$$.tmp
cat > txt.txt

echo "Content-type: text/plain"
echo ""

function error_response() {
  echo "Status: 400 Bad Request"
  echo "Content-type: text/plain"
  echo ""
  echo "ERROR: ${1}"
  rm -f $POST_CONTENT
  exit
}
###

if [ -n "$CONTENT_LENGTH" ]; then
  cat txt.txt > $POST_CONTENT;
else
  error_response "No content found on the request";
fi
###

if [ "$REQUEST_METHOD" != "POST" ]; then error_response "Invalid method. The only supported method is POST."; fi
###

VAT=$(cut -d "&" -f 1 $POST_CONTENT | cut -d "=" -f 2);
if [ -z "$VAT" ]; then error_response "Vat is empty"; fi
if [ ${#VAT} != 9 ]; then error_response "Vat has to be a 9 digit number"; fi

FIRSTNAME=$(cut -d "&" -f 2 $POST_CONTENT | cut -d "=" -f 2);
if [ -z "$FIRSTNAME" ]; then error_response "First Name is empty"; fi

LASTNAME=$(cut -d "&" -f 3 $POST_CONTENT | cut -d "=" -f 2);
if [ -z "$LASTNAME" ]; then error_response "Last Name is empty"; fi

ADDRESS=$(cut -d "&" -f 4 $POST_CONTENT | cut -d "=" -f 2);
if [ -z "$ADDRESS" ]; then error_response "Address is empty"; fi

INVDATE=$(cut -d "&" -f 5 $POST_CONTENT | cut -d "=" -f 2);
if [ -z "$INVDATE" ]; then error_response "Invoice date is empty"; fi

MONTHSUPPLY=$(cut -d "&" -f 6 $POST_CONTENT | cut -d "=" -f 2);
if [ -z "$MONTHSUPPLY" ]; then error_response "Month of Supply is empty"; fi

AMOUNT=$(cut -d "&" -f 7 $POST_CONTENT | cut -d "=" -f 2);
if [ -z "$AMOUNT" ]; then error_response "Amount is empty"; fi
if (( $AMOUNT <= 0 )); then error_response "Amount needs to be positive"; fi

INVOICE=$(ls -l /home/public-utility/database/"$VAT" | wc -l );

echo "Vat="$VAT >> temp.txt
echo "FirstName="$FIRSTNAME >> temp.txt
echo "Lastname="$LASTNAME >> temp.txt
echo "Address="$ADDRESS >> temp.txt
echo "InvDate="$INVDATE >> temp.txt
echo "MonthSupply="$MONTHSUPPLY >> temp.txt
echo "Amount="$AMOUNT >> temp.txt

#XXX=$(cat temp.txt)
#echo $XXX

if [ -d /home/public-utility/database/"$VAT" ]; then
  cat temp.txt >> /home/public-utility/database/"$VAT"/"$INVOICE";
else
  error_responde "VAT not found";
fi
###

rm -f $POST_CONTENT
rm -f txt.txt
rm -f temp.txt

echo "Invoice successfully registered"
###
