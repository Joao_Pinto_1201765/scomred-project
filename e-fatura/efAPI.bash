#! /bin/bash

POST_CONTENT=/tmp/APIrequest.$$.tmp

function error_response () {
echo "Status: 400 Bad Request"
echo "Content-type: text/plain"
echo ""
echo "Error: ${1}"
rm -f $POST_CONTENT
exit
}



if [ -n "$CONTENT_LENGTH" ]; then cat > $POST_CONTENT;
        else error_response "No content found on the request"; fi

if [ "$REQUEST_METHOD" != "POST" ]; then error_response "Invalid method. The only supported method POST";
        fi

VAT=$(cut -d "=" -f 2 $POST_CONTENT)

if [ -z "$VAT" ]; then error_response "Empty VAT field"; fi
if test ! -d /home/e-fatura/database/"$VAT"/; then error_response "Not a registered VAT"; fi

REQUESTED_INFO=/tmp/RequestedInfo.$$.tmp

for x in /home/e-fatura/database/"$VAT"/*; do

DATE=$(cut -d "&" -f 5 $x)
AMOUNT=$(cut -d "&" -f 4 $x)
SERVICE_PROVIDER_VAT=$(cut -d "&" -f 3 $x)
INVOICE_NUMBER=$(cut -d "&" -f 1 $x)


echo $DATE"&"$AMOUNT"&"$SERVICE_PROVIDER_VAT"&"$INVOICE_NUMBER >> $REQUESTED_INFO;
done

echo "Content-type: text/plain"
echo ""
cat $REQUESTED_INFO

rm -f $POST_CONTENT
rm -f $REQUESTED_INFO
