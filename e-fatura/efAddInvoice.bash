#!/bin/bash

POST_CONTENT=/tmp/.AddInvoice.$$.tmp

###

function error_response() {
 echo "Status: 400 Bad Request"
 echo "Content-type: text/plain"
 echo ""
 echo "ERROR: ${1}"
 rm -f $POST_CONTENT
 exit
}

###

if [ -n "$CONTENT_LENGTH" ]; then cat > $POST_CONTENT;


        else error_response "No content found on the request"; fi

if [ "$CONTENT_LENGTH" == "0" ]; then error_response "No content found on the request"; fi

###

if [ "$REQUEST_METHOD" != "POST" ]; then error_response "Invalid method. The only supported method is POST."; fi

###

NAME=$( cut -d "&" -f 1 $POST_CONTENT | cut -d "=" -f 2 );
VAT=$( cut -d "&" -f 2 $POST_CONTENT | cut -d "=" -f 2 );
PROVIDER_VAT=$( cut -d "&" -f 3 $POST_CONTENT | cut -d "=" -f 2);
AMOUNT=$( cut -d "&" -f 4 $POST_CONTENT | cut -d "=" -f 2);
DATE=$( cut -d "&" -f 5 $POST_CONTENT | cut -d "=" -f 2);

if [ -z "$NAME" ] || [ -z "$VAT" ] || [ -z "$PROVIDER_VAT" ] || [ -z "$AMOUNT" ] || [ -z "$DATE" ]; then error_response "Input parametres should not be empty"; fi
if [ ${#VAT} != 9 ] || [ ${#PROVIDER_VAT} != 9 ]; then error_response "Invalid VAT. VAT should be a 9 digit long number"; fi
if [ "$NAME" -lt 0 ]; then error_response "Invalid Invoice Number. Invoice number should be a positive number."; fi
if [ "$AMOUNT" -le 0 ]; then error_response "Invalid Amount. Amount should be greater than 0"; fi

if [ -d /home/e-fatura/database/"$VAT" ]; then
if [ -e /home/e-fatura/database/"$VAT"/"$NAME" ]; then error_response "Invoice already registered"; fi
cat $POST_CONTENT > /home/e-fatura/database/"$VAT"/"$NAME";
else error_response "VAT not registered"; fi

###

# rm -f $POST_CONTENT
echo "Content-type: text/plain";
echo "";
echo "Invoice successfully registered";

rm -f $POST_CONTENT;
###
