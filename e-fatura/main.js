function displayAddVat() {
    document.getElementById("AddVat").style.display = "block";
    document.getElementById("addTaxable").style.display = "none";
    document.getElementById("showAPI").style.display = "none";
    document.getElementById("addInvoice").style.display = "none";
}

function displayInsertInvoice() {
    document.getElementById("addTaxable").style.display = "none";
    document.getElementById("addInvoice").style.display = "none";
    document.getElementById("showAPI").style.display = "none";
    document.getElementById("invoice").style.display = "block";
}

function displayAPI() {
    document.getElementById("addTaxable").style.display = "none";
    document.getElementById("addInvoice").style.display = "none";
    document.getElementById("showAPI").style.display = "none";
    document.getElementById("API").style.display = "block";
}

function back() {
    document.getElementById("AddVat").style.display = "none";
    document.getElementById("invoice").style.display = "none";
    document.getElementById("API").style.display = "none";
    document.getElementById("addTaxable").style.display = "block";
    document.getElementById("addInvoice").style.display = "block";
    document.getElementById("showAPI").style.display = "block";
    clear();
}

function clear() {
    document.getElementById("Vat").value = '';
    document.getElementById('InvoiceNumber').value = '';
    document.getElementById('ConsumerVat').value = '';
    document.getElementById('ServiceProviderVat').value = '';
    document.getElementById('Amount').value = '';
    document.getElementById('Date').value = '';
    document.getElementById('invoiceResult').innerHTML = '';
    document.getElementById('vatResult').innerHTML = '';
}

function isEmptyOrBlank(string) {
    return string == null || string.trim() === '';
}



function validateVAT(VATLength) {
    if (VATLength != 9) {
        return false;
    } else {
        return true
    }
}

function addVat() {

    var request = new XMLHttpRequest();
    var VAT = document.getElementById("Vat").value;
    var data = "Vat=" + VAT;
    var VATLength = VAT.toString().length;
    if (isEmptyOrBlank(VAT)) {
        document.getElementById("vatResult").innerHTML = "Vat should not be empty";
    } else if (!validateVAT(VATLength)) {
        document.getElementById("vatResult").innerHTML = "Invalid VAT size. It must be a number with 9 digits";
    } else if (!validatePositive(VAT)) {
        document.getElementById("vatResult").innerHTML = "Invalid VAT. VAT must be a positive number with 9 digits";
    } else {
        request.onload = function okCase() {
            document.getElementById("vatResult").innerHTML = this.responseText;
        }
        request.onerror = function errorCase() {
            document.getElementById("vatResult").innerHTML = "Network error";
        }
        request.ontimeout = function onTimeOut() {
            document.getElementById("vatResult").innerHTML = "No response from web service";
        }
        request.open("POST", "/cgi-bin/efAddVat.bash", true);
        request.timeout = 5000;
        request.send(data);
    }
}

function validatePositive(number) {
    return number > 0;
}


function addInvoice() {
    var request = new XMLHttpRequest();
    var InvoiceNumber = document.getElementById("InvoiceNumber").value;
    var ConsumerVat = document.getElementById("ConsumerVat").value;
    var ProviderVat = document.getElementById("ServiceProviderVat").value;
    var Amount = document.getElementById("Amount").value;
    var Date = document.getElementById("Date").value;
    var data = "InvoiceNumber=" + InvoiceNumber + "&Vat=" + ConsumerVat + "&VatServiceProvider=" + ProviderVat + "&Amount=" + Amount + "&Date=" + Date;

    var consumerVATLength = document.getElementById("ConsumerVat").value.toString().length;
    var serviceProviderVAT = document.getElementById("ServiceProviderVat").value.toString().length;
    if (isEmptyOrBlank(InvoiceNumber) || isEmptyOrBlank(ConsumerVat) || isEmptyOrBlank(ProviderVat) || isEmptyOrBlank(Amount) || isEmptyOrBlank(Date)) {
        document.getElementById("invoiceResult").innerHTML = "Input fields should not be empty";
    } else if (!validateVAT(consumerVATLength)) {
        document.getElementById("invoiceResult").innerHTML = "Invalid consumer VAT size. It must be a number with 9 digits";
    } else if (!validateVAT(serviceProviderVAT)) {
        document.getElementById("invoiceResult").innerHTML = "Invalid Service Provider VAT size. It must be a number with 9 digits";
    } else if (!validatePositive(ConsumerVat)) {
        document.getElementById("invoiceResult").innerHTML = "Invalid consumer VAT. VAT must be a positive number with 9 digits";
    } else if (!validatePositive(ProviderVat)) {
        document.getElementById("invoiceResult").innerHTML = "Invalid Service Provider VAT. VAT must be a positive number with 9 digits";
    } else if (!validatePositive(InvoiceNumber)) {
        document.getElementById("invoiceResult").innerHTML = "Invalid Invoice Number. Must be greater than zero."
    } else if (!validatePositive(Amount)) {
        document.getElementById("invoiceResult").innerHTML = "Invalid Amount. Must be greater than zero."
    } else {
        request.onload = function okCase() {
            document.getElementById("invoiceResult").innerHTML = this.responseText;
        }
        request.onerror = function errorCase() {
            document.getElementById("invoiceResult").innerHTML = "Network error";
        }
        request.ontimeout = function onTimeOut() {
            document.getElementById("invoiceResult").innerHTML = "No response from web service";
        }
        request.open("POST", "/cgi-bin/efAddInvoice.bash", true);
        request.timeout = 5000;
        request.send(data);
    }
}


