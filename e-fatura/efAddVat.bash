#!/bin/bash

POST_CONTENT=/tmp/.AddTaxablePerson.$$.tmp

###

function error_response() {
 echo "Status: 400 Bad Request"
 echo "Content-type: text/plain"
 echo ""
 echo "ERROR: ${1}"
 rm -f $POST_CONTENT
 exit
}

###

if [ -n "$CONTENT_LENGTH" ]; then cat > $POST_CONTENT;
 else error_response "No content found on the request"; fi

###

if [ "$REQUEST_METHOD" != "POST" ]; then error_response "Invalid method. The only supported method is POST."; fi

###

NAME=$( cut -d "=" -f 2 $POST_CONTENT );

if [ -z "$NAME" ]; then error_response "Empty VAT field"; fi
if [ ${#NAME} != 9 ]; then error_response "Invalid VAT. VAT should be a 9 digit number"; fi

if [ -d /home/e-fatura/database/"$NAME" ]; then error_response "Vat already registered"; fi
mkdir /home/e-fatura/database/"$NAME"

###

# rm -f $POST_CONTENT
echo "Content-type: text/plain";
echo "";
echo "Vat successfully registered";

rm -f $POST_CONTENT;
###

