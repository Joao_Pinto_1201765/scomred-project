function addFamilyForm() {

    var txt = "Name: </td><td><input type='text' id='Name'></td></tr></br>" +
        "<tr><td>VAT Number: </td><td><input type='text' id='VAT'></td></tr></br>" +
        "<tr><td> CC Number: </td><td><input type='text' id='CC'></td></tr></br>" +
        "<tr><td>Birth Date: </td><td><input type='date' id='bdate'></td></tr></br>" +
        "<tr><td>Email Address: </td><td><input type='email' id='email'></td></tr></br>" +
        "<tr><td>Phone Number </td><td><input type='number' id='phone'></td></tr></br>" +
        "<tr><td><input type='button' value='SEND' onclick='addFamily()'>";
    document.getElementById("famForm").innerHTML = txt;
}

function addFamily() {
    const name = document.getElementById("Name").value;
    const vat = document.getElementById("VAT").value;
    const cc = document.getElementById("CC").value;
    const bdate = document.getElementById("bdate").value;
    const email = document.getElementById("email").value;
    const phone = document.getElementById("phone").value;

    var data = "NAME=" + name + "&" + "VATNUMBER=" + vat + "&" + "CC=" + cc + "&" + "BIRTHDATE=" + bdate + "&" + "EMAIL=" + email + "&" + "PHONE_NUMBER=" + phone;
    var request = new XMLHttpRequest();

    request.onload = function okCase() {
        var result;
        if (this.status == 200) {
            result = this.responseText;
            document.getElementById("famForm").innerHTML = result;
        } else {
            result = "<p>Error " + this.status + " : " + this.responseText + "</p>"
            document.getElementById("famForm").innerHTML = result;
        }


    }
    request.onerror = function errorCase() {
        document.getElementById("famForm").innerHTML = "Network error";
    }
    request.ontimeout = function onTimeout() {
        document.getElementById("famForm").innerHTML = "No response from web service";
    }

    request.open("POST", "/cgi-bin/addFamilyMember.bash", true);
    request.timeout = 5000;
    request.send(data);

}

function connectServiceForm() {

    document.getElementById("apiForm").innerHTML = "<p>VAT Number: <input type='text' id='VATapi'></br>" +
        "E-Facturas: <input type='radio' id='1' name='api' value='efacturas'></br>" +
        "Financial Services: <input type='radio' id='2' name='api' value='finServ'></br>" +
        "Public Utility: <input type='radio' id='3' name='api' value='pubUt'></br>" +
        "<input type='url' id='apitxt'><input type='button' value='Connect' onclick='submitAPI()'></p>";

}

function submitAPI() {
    const vat = document.getElementById("VATapi").value;
    var apis = document.getElementsByName('api');
    var api
    for (let i = 0; i < apis.length; i++) {
        if (apis[i].checked) {
            api = apis[i].value;
        }

    }

    var url = document.getElementById("apitxt").value;
    var data = "VAT=" + vat + "&API=" + api + "&URL=" + url;

    var request = new XMLHttpRequest();

    request.onload = function okCase() {
        var result;
        if (this.status == 200) {
            result = this.responseText;
            document.getElementById("apiForm").innerHTML = result;
        } else {
            result = "<p>Error " + this.status + " : " + this.responseText + "</p>"
            document.getElementById("apiForm").innerHTML = result;
        }
    }

    request.onerror = function errorCase() {
        document.getElementById("apiForm").innerHTML = "Network error";
    }
    request.ontimeout = function onTimeout() {
        document.getElementById("apiForm").innerHTML = "No response from web service";
    }

    request.open("POST", "/cgi-bin/connectToAPI.bash", true);
    request.timeout = 5000;
    request.send(data);
}

function retrieveAPIList() {

    var vat = document.getElementById("getVAT").value;
    var request = new XMLHttpRequest();
    request.onload = function okCase() {
        var result;
        if (this.status == 200) {
            result = this.responseText;
            document.getElementById("importForm").innerHTML = result + "<input type='button' value='Retrieve Data' onclick='connectAPI()'>";
        } else {
            result = "<p>Error " + this.status + " : " + this.responseText + "</p>"
            document.getElementById("importForm").innerHTML = result;
        }


    }
    request.onerror = function errorCase() {
        document.getElementById("importForm").innerHTML = "Network error";
    }
    request.ontimeout = function onTimeout() {
        document.getElementById("importForm").innerHTML = "No response from web service";
    }

    request.open("POST", "/cgi-bin/retrieveAPIList.bash", true);
    request.timeout = 5000;
    request.send(vat);

}
