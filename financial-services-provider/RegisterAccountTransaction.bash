#!/bin/bash
POST_CONTENT=/tmp/.registerAccountTransaction$$.tmp

function errorMessage() {
    echo "Status: 400 Bad Request"
    echo "Content-type: text/plain"
    echo ""
    echo "ERROR: ${1}"

    rm -f $POST_CONTENT
    exit
}

if [ -n "$CONTENT_LENGTH" ]; then read QUERY_STRING_POST > $POST_CONTENT;
        echo $QUERY_STRING_POST > $POST_CONTENT

    else error_response "No content found on the request"; fi

if [ "$CONTENT_LENGTH" == "0" ]; then errorMessage "No content found on the request"; fi

if [ "$REQUEST_METHOD" != "POST" ]; then errorMessage "Method not supported, the only one is POST."; fi

VAT=$( cut -d "&" -f 1  $POST_CONTENT | cut -d "=" -f 2 );

if [ ${#VAT} != 9 ]; then errorMessage "Invalid VAT. VAT should be a 9 digit long number"; fi


if test ! -d /home/financial-service-provider/database/$VAT/ ; then errorMessage "Account doesn´t exist"; fi

# date +"%T.%N' -> data com milisegundos
TEMP=$( date +"%T.%N" );

TEMPTWO=$( echo "transaction" );

AUX=${TEMPTWO}${TEMP};


cat $POST_CONTENT > /home/financial-service-provider/database/$VAT/${AUX};

echo "Content-type: text/plain";
echo "";
echo "Transaction added with success";

rm -f $POST_CONTENT;