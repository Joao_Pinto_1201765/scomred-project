# Dados de Input

## Dados para Add Account

| Dado | id | Obersações |
|------|----|------------|
| Titular da Conta | titular | nome da pessoa alvo |
| VATNumber | vat | VAT da pessoa alvo |
| País de domicialiação bancária | pais | |
| Domiciliação | banco | nome do banco |
| IBAN | iban | IBAN da conta a adicionar |
|------|-----|-----------|
| botão | submit | meter o id do botão como submit |



## Dados para Resgister Account Transaction 

| Dado | id | Obersações |
|------|----|------------|
| VAT | vat | vat da pessoa alvo |
| Detalhe | detalhe | detalhe do movimento |
| Data da operação | dataOperacao | data do movimento |
| Data do valor | dataValor | data do valor |
| Valor | valor | valor da transacção |
|------|-----|-----------|
| botão | submit | meter o id do botão como submit |