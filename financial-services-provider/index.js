// https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_toggle_hide_show


function show(element) {
    var x = document.getElementById(element);
    x.style.display = "block";
}

function hide(element) {
    var x = document.getElementById(element);
    x.style.display = "none";
}

function clearValidationResponse() {
    document.getElementById('account-created').value = "";
    document.getElementById('transaction-created').value = "";
}

function clearServerResponse() {
    document.getElementById('account-response').value = "";
    document.getElementById('transaction-response').value = "";
}

function createAccount() {

    var request = new XMLHttpRequest();

    var owner = document.getElementById("owner").value;
    var vataccount = document.getElementById("vataccount").value;
    var country = document.getElementById("country").value;
    var bank = document.getElementById("bank").value;
    var iban = document.getElementById("iban").value;

    if (owner == "" || owner == null) {
        document.getElementById('account-validation').innerHTML = "Name must not be empty or null"
    } else if (vataccount.length > 9 || vataccount.length < 9 || vataccount == "") {
        document.getElementById('account-validation').innerHTML = "Vat number is invalid or empty"
    } else if (country == "" || country == null) {
        document.getElementById('account-validation').innerHTML = "Country must not be empty or null"
    } else if (bank == "" || bank == null) {
        document.getElementById('account-validation').innerHTML = "Bank must not be empty or null"
    } else if (iban == null || iban == "" || iban.length > 24 || iban.length < 23) {
        document.getElementById('account-validation').innerHTML = "IBAN number is invalid"
    } else {
        var data = "owner=" + owner + "&vataccount=" + vataccount + "&country=" + country + "&bank=" + bank + "&iban=" + iban;

        request.open("POST", "/cgi-bin/CreateAccount.bash", true);
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        request.timeout = 5000;
        request.send(data);

        request.onload = function success() {
            document.getElementById("account-response").innerHTML = this.responseText;
        }

        request.onerror = function failure() {
            document.getElementById("account-response").innerHTML = this.responseText;
        }

        request.ontimeout = function timeOut() {
            document.getElementById("account-response").innerHTML = this.responseText;
        }
    }
}

function clearAccountData() {

    document.getElementById("owner").value = "";
    document.getElementById("vataccount").value = "";
    document.getElementById("country").value = "";
    document.getElementById("bank").value = "";
    document.getElementById("iban").value = "";
}

function registerAccountTransaction() {

    var request = new XMLHttpRequest();

    var vattransaction = document.getElementById("vattransaction").value;
    var description = document.getElementById("description").value;
    var operationdate = document.getElementById("operationdate").value;
    var valuedate = document.getElementById("valuedate").value;
    var amount = document.getElementById("amount").value;

    if (vattransaction.length > 10 || vattransaction.length < 9 || vattransaction == "") {
        document.getElementById('transaction-validation').innerHTML = "Vat number is invalid"
    } else if (description == "" || description == null) {
        document.getElementById('transaction-validation').innerHTML = "Description must not be empty or null"
    } else if (operationdate == null || operationdate.length < 1) {
        document.getElementById('transaction-validation').innerHTML = "Date must not be empty or null"
    } else if (valuedate == "" || valuedate == null) {
        document.getElementById('transaction-validation').innerHTML = "Date must not be empty or null"
    } else if (amount == null || amount == "" || amount < 0) {
        document.getElementById('transaction-validation').innerHTML = "Amount must not be empty, null or negative"
    } else {
        var data = "vattransaction=" + vattransaction + "&description=" + description + "&operationdate=" + operationdate + "&valuedate=" + valuedate + "&amount=" + amount;

        request.open("POST", "/cgi-bin/RegisterAccountTransaction.bash", true);
        request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        request.timeout = 5000;
        request.send(data);

        request.onload = function success() {
            document.getElementById("transaction-response").innerHTML = this.responseText;
        }

        request.onError = function failure() {
            document.getElementById("transaction-response").innerHTML = this.responseText;
        }

        request.ontimeout = function timeOut() {
            document.getElementById("transaction-response").innerHTML = this.responseText;
        }
    }
}

function clearTransactionData() {

    document.getElementById("vattransaction").value = "";
    document.getElementById("description").value = "";
    document.getElementById("operationdate").value = "";
    document.getElementById("valuedate").value = "";
    document.getElementById("amount").value = "";
}

function haide(element) {
    var e = document.getElementById(element);
    var accountserverresponse = document.getElementById('account-response');
    var transactionserverresponse = document.getElementById('transaction-response');
    e.style.display = "none"
    e.style.zIndex = -1;
    accountserverresponse.innerHTML = '';
    transactionserverresponse.innerHTML = '';
}




