# API DOCUMENTATION

## API

The **Financial Service Provider** provides a **REST Web API** that allows you to access the data of an account present in our system. This API covers access to existing accounts in our system and to their transactions, allowing the consultation of data. In this way it is possible to create solutions that **integrate** the Financial Service Provider **with any other system**.

## GLOBAL VISION

The Financial Service Provider API is a **RESTful Web API** that allows you to access customer data. This API provides access to customer data within your accounts.

------------
### ***The URL structure and XMLHttpRequest:***

https://vs186.dei.isep.ipp.pt/cgi-bin/FSP_API.bash

------------
**{vat}** - identifies the vat of the account to be consulted

````javascript
var data = "vat={vat}";

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
  if(this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "https://vs186.dei.isep.ipp.pt/cgi-bin/FSP_API.bash");
xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

xhr.send(data);
````

The data of the consultations carried out are returned in a text.

The only HTTP request method available is POST, allowing you to obtain account data.

### **Exemple:**

Get transactions for a given account

**Data:**

- vat = 123456789

**URL:**

POST /cgi-bin/FSP_API.bash

**POST BODY**

````javascript
var data = "vat=123456789";

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
  if(this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "https://vs186.dei.isep.ipp.pt/cgi-bin/FSP_API.bash");
xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

xhr.send(data);
````

**Answer:**

```
date=12-10-2020&amount=1&valuedate=12-10-2020&description=Shopping
date=12-10-2020&amount=1&valuedate=12-10-2020&description=Shopping
date=12-10-2020&amount=1&valuedate=12-10-2020&description=Shopping
date=12-10-2020&amount=2&valuedate=12-10-2020&description=Shopping
date=12-10-2021&amount=2&valuedate=12-10-2020&description=Jantar
date=12-10-2021&amount=2&valuedate=12-10-2020&description=Combustivel
```