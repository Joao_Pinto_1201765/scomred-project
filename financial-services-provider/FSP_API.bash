#!/bin/bash
POST_CONTENT=/tmp/FPS_API_ACCESS$$.tmp
ENDRESULT=/tmp/result$$.tmp

function errorMessage() {
    echo "Status: 400 Bad Request"
    echo "Content-type: text/plain"
    echo ""
    echo "ERROR: ${1}"

    rm -f $POST_CONTENT
    rm -f $ENDRESULT
    exit
}

if [ -n "$CONTENT_LENGTH" ]; then
  cat > $POST_CONTENT;
else
  errorMessage "No content found on the request";
fi

if [ "$REQUEST_METHOD" != "POST" ]; then
 error_response "Invalid method. The only supported method is POST.";
fi

VAT=$( cut -d "=" -f 2 $POST_CONTENT )
if [ ${#VAT} != 9 ]; then errorMessage "Invalid VAT. VAT should be a 9 digit long number"; fi


if test ! -d /home/financial-service-provider/database/$VAT/ ; then errorMessage "Account doesn´t exist."; fi

for i in /home/financial-service-provider/database/$VAT/tr* ; do

  DESCRIPTION=$( cat $i | cut -d "&" -f 2 | cut -d "=" -f 2 );

  OPERATIONDATE=$( cut -d "&" -f 3 $i | cut -d "=" -f 2 );

  VALUEDATE=$( cut -d "&" -f 4 $i | cut -d "=" -f 2 );

  AMOUNT=$( cut -d "&" -f 5 $i | cut -d "=" -f 2 );

echo "date="${OPERATIONDATE}"&amount="${AMOUNT}"&valuedate="${VALUEDATE}"&description="${DESCRIPTION} >> $ENDRESULT;

done

echo "Content_type: text/plain"
echo ""
cat $ENDRESULT
