#!/bin/bash
POST_CONTENT=/tmp/addAccount$$.tmp

function errorMessage() {
    echo "Status: 400 Bad Request"
    echo "Content-type: text/plain"
    echo ""
    echo "ERROR: ${1}"

    rm -f $POST_CONTENT
    exit
}

if [ -n "$CONTENT_LENGTH" ]; then read QUERY_STRING_POST > $POST_CONTENT;
        echo $QUERY_STRING_POST > $POST_CONTENT

    else errorMessage "No content found on the request"; fi

if [ "$CONTENT_LENGTH" == "0" ]; then errorMessage "No content found on the request"; fi

if [ "$REQUEST_METHOD" != "POST" ]; then errorMessage "Method not supported, the only one is POST."; fi

NAME=$( cut -d "&" -f 2 $POST_CONTENT | cut -d "=" -f 2 );

if [ ${#VAT} != 9 ]; then errorMessage "Invalid VAT. VAT should be a 9 digit long number"; fi

if [ -d /home/financial-services-provider/database/$NAME/ ]; then errorMessage "Account already exists"; fi

mkdir /home/financial-services-provider/database/$NAME;

cat $POST_CONTENT > /home/financial-services-provider/database/$NAME/AccountInfo;

# inicio da mensagem
echo "Content-type: text/plain";
echo "";
echo "Account added with success";

rm -f $POST_CONTENT; fi